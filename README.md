# Prestine Cabs

This is a taxi booking website made by Team 12 for Assignment 2 - MCD 4290.

## Features
- Use your current location for pickup
- Add stop over destinations
- Search your final destination
- Pick your date and time
- View the distance in meters
- Drag and adjust the markers for your destinations
- Change taxi type and view cost in the summary page
- See your past bookings

## Please Keep in mind while testing
- Please add stop overs at the end i.e. after your choose your final destination and current location

- When selecting your final destination please use the find location button and choose your 
  location from the list provided, you will still be able to adjust the marker by dragging it.


## Preview

The following images might be from a few versions before and might not be up to date. Please don't rely on them for accurate information.

![Index](https://bitbucket.org/Rishi-Monash/team-12/raw/d622e2192385bc395c0d21a78cfadf22deea04bb/images/preview-index.png)

![Summary](https://bitbucket.org/Rishi-Monash/team-12/raw/e3d207919803b0b356edaf2173bc5e711db82a3e/images/summary.png)

![History](https://bitbucket.org/Rishi-Monash/team-12/raw/e3d207919803b0b356edaf2173bc5e711db82a3e/images/history.png)