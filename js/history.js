const allData = retrieveLSData("allData");
console.log(allData);

const tablebody = document.getElementById("tablebody");
for (let i = 0; i < allData.length; i++) {
    const row = `
                  <tr>
                      <td class="mdl-data-table__cell--non-numeric">${i + 1}</td>
                      <td>${allData[i].startName}</td>
                      <td>${allData[i].endName}</td>
                      <td>$${allData[i].cost}</td>
                      <th> FINISHED </th>
                  </tr>
`;
    tablebody.innerHTML += row;
}
