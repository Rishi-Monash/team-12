const currBookingData = retrieveLSData("currBooking");

const stoploc = document.getElementById("endlocation");
const costRef = document.getElementById("cost");
const startloc = document.getElementById("startlocation");
const taxiTypeRef = document.getElementById("taxitype");
const stopoverlist = document.getElementById("stopoverlist");
const regno = document.getElementById("regno");

const cost = fare(currBookingData.distance, currBookingData.taxiType, currBookingData.time);

console.log(currBookingData);
console.log(cost);
startloc.innerText = currBookingData.startName;
stoploc.innerText = currBookingData.endName;
taxiTypeRef.value = currBookingData.taxiType;

costRef.innerText = "$" + cost;

function addStopoverName(data) {
    console.log(data);
    const StopoverName = data.results[0].formatted;
    const html = "<li>" + StopoverName + "</li>";
    stopoverlist.innerHTML += html;
}

for (let i = 0; i < currBookingData.stopOvers.length; i++) {
    const currLat = currBookingData.stopOvers[i].lat;
    const currLng = currBookingData.stopOvers[i].lng;
    sendWebServiceRequestForReverseGeocoding(currLat, currLng, "addStopoverName");
}

for (let i = 0; i < taxiList.length; i++) {
    if (taxiList[i].type === taxiTypeRef.value && taxiList[i].available) {
        regno.innerText = taxiList[i].rego;
        break;
    }
}

function costForOtherTaxiTypes() {
    let costObj = {
        basictaxi: 0,
        suv: 0,
        van: 0,
        minibus: 0,
    };
    const dist = currBookingData.distance;
    const time = currBookingData.time;
    costObj.basictaxi = fare(dist, "basictaxi", time);
    costObj.suv = fare(dist, "suv", time);
    costObj.van = fare(dist, "van", time);
    costObj.minibus = fare(dist, "minibus", time);
    return costObj;
}

const costOfTaxis = costForOtherTaxiTypes();
console.log(costOfTaxis);

function changecost() {
    const taxitype = taxiTypeRef.value;
    costRef.innerText = "$" + costOfTaxis[taxitype].toFixed(2);
    currBookingData.taxiType = taxitype;
    for (let i = 0; i < taxiList.length; i++) {
        if (taxiList[i].type === taxitype && taxiList[i].available) {
            regno.innerText = taxiList[i].rego;
            break;
        }
    }
}

function updateData() {
    if (localStorage.getItem("allData") === null) {
        updateLSData("allData", [currBookingData]);
        // Doesn't exist
    } else {
        let AllData = retrieveLSData("allData");
        AllData.push(currBookingData);
        updateLSData("allData", AllData);
        // exists
    }
}

function submit() {
    if (confirm("Do you wish to confirm your booking?") === true) {
        //ok
        currBookingData["cost"] = costOfTaxis[taxiTypeRef.value].toFixed(2);
        currBookingData["rego"] = regno.innerText;
        updateData();
        window.location.href = "./index.html";
    } else {
        //cancel
    }
}
