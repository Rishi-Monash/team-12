"use strict";

/* Functions used from services.js
------------------------------------------
sendWebServiceRequestForReverseGeocoding(lat,lng,callback)
sendWebServiceRequestForForwardGeocoding(location, callback)'index2.js'



Using keys
------------------------------------------
MAPBOX_KEY
OPENCAGE_KEY => in services.js */

// DOM Elements
const pickCurrLocBut = document.querySelector("#pickCurrLoc");
const inputPickup = document.querySelector("#location1");
const inputPickupParent = document.querySelector(".mdl-textfield");

const inputFinal = document.querySelector("#final");
const findFinal = document.querySelector("#findFinal");

const stopOvers = document.querySelector("#stopovers"); // locations
const addStopovers = document.querySelector("#addStopOver"); // button

const distance = document.querySelector("#distance");

const submit = document.querySelector("#submit");

// Classes
class Booking {
    constructor() {
        this._startLocation = "";
        this.startLocationName = "";
        this.endLocation = "";
        this.endLocationName = "";
        this.stopOvers = [];
        this._taxiType = "";
        this._fare = "";
        this._time = "";
        this._distance = "";
        this.marker = "";
        this._currMarkerSet = true;
    }
    set startLocation(input) {
        if (typeof input === "object") {
            this._startLocation = input;
        } else {
            console.log("incorrect type");
        }
    }
    set taxiType(input) {
        if (typeof input === "string") {
            this._taxiType = input;
        } else {
            return "incorrect type";
        }
    }
    set distance(input) {
        if (typeof input === "number") {
            this._distance = input;
        } else {
            return "incorrect type";
        }
    }
    set fare(input) {
        if (typeof input === "number") {
            this._fare = input;
        } else {
            return "incorrect type";
        }
    }
    set currMarkerSet(value) {
        if (typeof value == "boolean") {
            this._currMarkerSet = value;
        }
    }
    get currMarkerSet() {
        return this._currMarkerSet;
    }

    confirmStop() {
        const coords = this.marker.getLngLat();
        this.marker.setDraggable(false);
        console.log(coords);
        let ids = [0];
        for (let i = 0; i < this.stopOvers.length; i++) {
            ids.push(this.stopOvers[i].id);
        }
        console.log(Math.max(...ids));
        this.stopOvers.push({
            id: Math.max(...ids) + 1,
            lat: coords.lat,
            lng: coords.lng,
            marker: this.marker,
        });
        this.marker.getPopup().setHTML(
            `
                <h6>Stopover</h6>
                <button onclick="deleteMarker(${
                    this.stopOvers[this.stopOvers.length - 1].id
                })">Delete</button>`
        );
        this.currMarkerSet = true;
        console.log(this);
        if (this._startLocation && this.endLocation) {
            const dist = totalDistance(this._startLocation, this.endLocation, this.stopOvers);
            this._distance = dist;
            distance.innerText = dist + "m";
        } else {
            console.log("No");
        }
    }
}

// Class initialise
let currBooking = new Booking();

// constants
const checkNavigator = "geolocation" in navigator;
const Setup = {
    map: "",
    currentLoc: {},
    pickupLoc: false,
    dropoffLoc: false,
};

getUserCurrentLocationUsingGeolocation((lat, lng) => {
    Setup.currentLoc = { lng, lat };
    mapboxgl.accessToken = MAPBOX_KEY;
    Setup.map = new mapboxgl.Map({
        container: "map",
        style: "mapbox://styles/mapbox/streets-v10",
        zoom: 16,
        center: [Setup.currentLoc.lng, Setup.currentLoc.lat],
    });
});

// DOM Manipulation
pickCurrLocBut.addEventListener("click", () => {
    if (!Setup.pickupLoc) {
        sendWebServiceRequestForReverseGeocoding(
            Setup.currentLoc.lat,
            Setup.currentLoc.lng,
            "pickCurrLoc"
        );
        Setup.pickupLoc = true;
    } else {
        Setup.map.panTo([Setup.currentLoc.lng, Setup.currentLoc.lat]);
    }
});

findFinal.addEventListener("click", () => {
    const inputFinalData = inputFinal.value;
    sendWebServiceRequestForForwardGeocoding(inputFinalData, "dispFinalStop");
    inputFinal.value = "";
});

addStopovers.addEventListener("click", () => {
    addStopover(Setup.map, Setup.currentLoc.lng, Setup.currentLoc.lat);
});

submit.addEventListener("click", () => {
    currBooking._time = dateControl.value;
    const stopOversWithoutMarker = currBooking.stopOvers.map((elem) => {
        delete elem.marker;
        return elem;
    });
    console.log(stopOversWithoutMarker);

    const finalObj = {
        start: currBooking._startLocation,
        startName: currBooking.startLocationName,
        stopOvers: stopOversWithoutMarker,
        end: currBooking.endLocation,
        endName: currBooking.endLocationName,
        distance: currBooking._distance,
        taxiType: currBooking._taxiType,
        time: currBooking._time,
    };
    updateLSData("currBooking", finalObj);
    window.location.href = "./summary.html";
});

// =================

function pickCurrLoc(data) {
    inputPickupParent.innerHTML = `<div id="inputPickup">${data.results[0].formatted}</div>`;
    currBooking.startLocationName = data.results[0].formatted;
    currBooking.startLocation = Setup.currentLoc;
    console.log(currBooking);
    const pickupmarker = new mapboxgl.Marker({
        draggable: true,
    })
        .setLngLat([Setup.currentLoc.lng, Setup.currentLoc.lat])
        .addTo(Setup.map);

    pickupmarker.on("dragend", () => {
        const lngLat = pickupmarker.getLngLat();
        currBooking.startLocation = {
            lng: lngLat.lng,
            lat: lngLat.lat,
        };
        console.log(currBooking);
    });
    console.log(data.results);
}

// Stop Over functions
function addStopover(map, lng, lat) {
    if (currBooking.currMarkerSet) {
        currBooking.currMarkerSet = false;
        const marker = new mapboxgl.Marker({
            draggable: true,
            color: "#fec601ff",
        })
            .setLngLat([lng, lat])
            .addTo(map);
        currBooking.marker = marker;
        const html = `<h6>Stopover</h6><button onclick="currBooking.confirmStop()">confirm</button>
        `;
        marker.setPopup(new mapboxgl.Popup().setHTML(html)); // add popup
    } else {
        alert("Please Confirm previous marker");
    }
}

function deleteMarker(id) {
    let tempStopovers = [];
    for (let i = 0; i < currBooking.stopOvers.length; i++) {
        if (currBooking.stopOvers[i].id !== id) {
            tempStopovers.push(currBooking.stopOvers[i]);
        } else {
            currBooking.stopOvers[i].marker.remove();
        }
    }
    currBooking.stopOvers = tempStopovers;
}

// Final Stop Functions

/**
 * dispFinalStop - Callback for the forward geocoding when you click on
 * Find location
 *
 * @param  {jsonp} data descriptio
 * @return {type}      description
 */
function dispFinalStop(data) {
    const placesDataList = document.querySelector("#places");
    placesDataList.innerHTML = "";
    // console.log(data.results);
    data.results.forEach((place, i) => {
        const option = `<option value="${place.formatted}">`;
        placesDataList.innerHTML += option;
    });
    inputFinal.oninput = () => {
        finalOnInput(data.results);
    };
}

/**
 * finalOnInput - This function will be used to add the final stop
 * for the destinations
 *
 * @return {type}  description
 */
function finalOnInput(data) {
    const inputFinalData = inputFinal.value;
    const dataListElems = document.querySelector("#places").childNodes;
    dataListElems.forEach((item, i) => {
        if (item.value === inputFinalData && !Setup.dropoffLoc) {
            console.log("from datalist", data[i]);
            currBooking.endLocationName = data[i].formatted;
            currBooking.endLocation = {
                lat: data[i].geometry.lat,
                lng: data[i].geometry.lng,
            };
            const endLocationMarker = new mapboxgl.Marker({
                draggable: true,
            })
                .setLngLat([currBooking.endLocation.lng, currBooking.endLocation.lat])
                .addTo(Setup.map);

            endLocationMarker.on("dragend", () => {
                const lngLat = endLocationMarker.getLngLat();
                currBooking.endLocation = {
                    lng: lngLat.lng,
                    lat: lngLat.lat,
                };
                console.log(currBooking);
            });
            Setup.dropoffLoc = true;
        } else if (Setup.dropoffLoc) {
            Setup.map.panTo([currBooking.endLocation.lng, currBooking.endLocation.lat]);
        }
    });
}

function selectTaxi(type) {
    currBooking._taxiType = type;
    console.log(currBooking);
}
