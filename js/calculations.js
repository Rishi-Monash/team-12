function fare(distance, taxitype, time) {
    const flatRate = 4.2;
    const commPassVehLevy = 1.1;
    const farePerKM = 1.622;
    const totalDistanceRate = parseInt(distance) * farePerKM;
    const vehicleRates = {
        basictaxi: 0,
        suv: 3.5,
        van: 6.0,
        minibus: 10.0,
    };
    const myVehicleRate = vehicleRates[taxitype];
    const bookingTime = new Date(time).getHours();
    let nightLevy = 0;
    if (bookingTime > 21 || bookingTime < 5) {
        nightLevy = 0.2 * flatRate;
    } else {
        nightLevy = 0;
    }
    console.log(nightLevy);
    console.log(myVehicleRate);
    const total = commPassVehLevy + flatRate + totalDistanceRate + myVehicleRate;
    return total;
}

function distBet2Points(start, end) {
    const r = 6371;
    const startLat = start.lat * (Math.PI / 180);
    const startLng = start.lng * (Math.PI / 180);
    const endLat = end.lat * (Math.PI / 180);
    const endLng = end.lng * (Math.PI / 180);
    const deltaLat = startLat - endLat;
    const deltaLng = startLng - endLng;

    const a =
        Math.pow(Math.sin(deltaLat / 2), 2) +
        Math.cos(startLat) * Math.cos(endLat) * Math.pow(Math.sin(deltaLng / 2), 2);
    console.log(a);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    console.log(c);
    const distance = r * c;
    return distance;
}

function totalDistance(startPoint, endPoint, stopOvers) {
    const arrOfAllpoints = [startPoint, ...stopOvers, endPoint];
    let distance = 0;
    for (let i = 0; i < arrOfAllpoints.length - 1; i++) {
        distance += distBet2Points(arrOfAllpoints[i], arrOfAllpoints[i + 1]);
    }
    const fixedDistance = (distance * 1000).toFixed(2);
    console.log(fixedDistance);
    return fixedDistance;
}
